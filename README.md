01) In Post.php  Model

	<?php

		namespace App;

		use Illuminate\Database\Eloquent\Model;
		use Illuminate\Database\Eloquent\SoftDeletes;

		class Post extends Model
		{
			use SoftDeletes;

			protected $table = "posts";

			protected $guard = [];

			protected $dates = [''];

		}

02) In migration

		Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('details');
            $table->timestamps();
            // $table->time('deleted_at')->nullable();
            $table->softDeletes();
        });

		
03) In Controller

	public function index()
    {
    	//delete data
    	// $post = Post::find(2);
    	// $post->delete();

    	// restore data
    	$post = Post::withTrashed()->find(1)->restore();

    	//fetch data
    	$post = Post::all();
    	
    	//        Product::withTrashed()->find(9)->restore();
        // Product::withTrashed()->find(10)->forceDelete();


    	//Show trash data
    	$trash = DB::table('posts')->whereNotNull('deleted_at')->get();

    	return view('home', compact('post','trash'));
    }







